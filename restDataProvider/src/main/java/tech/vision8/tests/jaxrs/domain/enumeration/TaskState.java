package tech.vision8.tests.jaxrs.domain.enumeration;

/**
 * The TaskState enumeration.
 */
public enum TaskState {
    PLANNED, STARTED, DONE, POSTPONNED
}
