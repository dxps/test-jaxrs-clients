/**
 * View Models used by Spring MVC REST controllers.
 */
package tech.vision8.tests.jaxrs.web.rest.vm;
