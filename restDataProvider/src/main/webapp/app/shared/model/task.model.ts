export const enum TaskState {
  PLANNED = 'PLANNED',
  STARTED = 'STARTED',
  DONE = 'DONE',
  POSTPONNED = 'POSTPONNED'
}

export interface ITask {
  id?: number;
  title?: string;
  description?: string;
  state?: TaskState;
}

export const defaultValue: Readonly<ITask> = {};
