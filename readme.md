# JAX-RS Clients Tests

The goal of this repo is to showcase the setup and behavior of Java-based JAX-RS clients.

Following the [JAX-RS standard](http://jax-rs-spec.java.net/) or not, the need is for using an Http client to interact with external endpoints that expose JSON content.

## Jersey Client

At the time of this writing, the latest version is 2.27.

This client has a quite a history behind:
- initially, version 1 was released by Sun
- later, it moved to GlassFish team and upgraded to [version 2](https://github.com/jersey/jersey)
- now it seems that will be part of [Eclipse umbrella](https://projects.eclipse.org/proposals/eclipse-jersey)

#### Playground Elements and Use Cases

For a quick test, you can use the following two elements:
- httpDataProvider
    - this is minimal mockup server
    - for starting up, see its readme
    - it can be accessed at `http://localhost:8000/api/tasks`
    - it returns a JSON array of some `Task` objects
- restDataConsumers
    - this project includes the Jersey specific setups and usage in `JerseyRestClientPool` and `GetTasksController`.
    - use `run-dev.sh` script to start it

restDataConsumers' `GetTasksController` is calling the httpDataProvider endpoint 
within two exposed GET operations:
- `/tasks-1`
    - it is using one single Jersey Client instance
- `/tasks-2`
    - it is using a Jersey Client pool

To simulate a realistic scenario, some delay (up to 1.5 sec) was introduced in the response of this controller's methods.

#### Behavior

For testing the Jersey Client behavior, you can use:
- `run-benchmark-restDataConsumers-t1.sh` and `run-benchmark-restDataConsumers-t2.sh` scripts
    - these are based on `ab` ([Apache Benchmarking tool](https://httpd.apache.org/docs/2.4/programs/ab.html))
- `run-test-restDataConsumers-t1.sh` and `run-test-restDataConsumers-t2.sh` scripts
    - these are sequentially using cURL for triggering the calls

##### Network Traffic

Checkout the network traffic between the Jersey Client (that uses Apache HttpClient) and the mockup server using the classic tcpdump approach.

Let's capture only the traffic that goes to the mockup server (the httpDataProvider component, who listens on port 8000 on loopback interface):
```bash
vision8@vision8:test-jaxrs-clients $ sudo tcpdump -i lo0 dst port 8000 -nn
Password:
tcpdump: verbose output suppressed, use -v or -vv for full protocol decode
listening on lo0, link-type NULL (BSD loopback), capture size 262144 bytes
22:07:37.422858 IP 127.0.0.1.53599 > 127.0.0.1.8000: Flags [S], seq 2984401960, win 65535, options [mss 16344,nop,wscale 5,nop,nop,TS val 755127641 ecr 0,sackOK,eol], length 0
22:07:37.422928 IP 127.0.0.1.53599 > 127.0.0.1.8000: Flags [.], ack 2627601098, win 12759, options [nop,nop,TS val 755127641 ecr 755127641], length 0
22:07:37.426688 IP 127.0.0.1.53599 > 127.0.0.1.8000: Flags [P.], seq 0:154, ack 1, win 12759, options [nop,nop,TS val 755127644 ecr 755127641], length 154
22:07:37.430508 IP 127.0.0.1.53599 > 127.0.0.1.8000: Flags [.], ack 656, win 12738, options [nop,nop,TS val 755127647 ecr 755127647], length 0
22:07:38.217878 IP 127.0.0.1.53604 > 127.0.0.1.8000: Flags [S], seq 183982805, win 65535, options [mss 16344,nop,wscale 5,nop,nop,TS val 755128433 ecr 0,sackOK,eol], length 0
22:07:38.217941 IP 127.0.0.1.53604 > 127.0.0.1.8000: Flags [.], ack 1626318575, win 12759, options [nop,nop,TS val 755128433 ecr 755128433], length 0
22:07:38.218644 IP 127.0.0.1.53605 > 127.0.0.1.8000: Flags [S], seq 1968156101, win 65535, options [mss 16344,nop,wscale 5,nop,nop,TS val 755128433 ecr 0,sackOK,eol], length 0
22:07:38.218700 IP 127.0.0.1.53605 > 127.0.0.1.8000: Flags [.], ack 1413284095, win 12759, options [nop,nop,TS val 755128433 ecr 755128433], length 0
22:07:38.218803 IP 127.0.0.1.53599 > 127.0.0.1.8000: Flags [P.], seq 154:308, ack 656, win 12738, options [nop,nop,TS val 755128434 ecr 755127647], length 154
22:07:38.219215 IP 127.0.0.1.53604 > 127.0.0.1.8000: Flags [P.], seq 0:154, ack 1, win 12759, options [nop,nop,TS val 755128434 ecr 755128433], length 154
22:07:38.219277 IP 127.0.0.1.53605 > 127.0.0.1.8000: Flags [P.], seq 0:154, ack 1, win 12759, options [nop,nop,TS val 755128434 ecr 755128433], length 154

.......(content omitted)....... 
```

In this tcpdump output, there are three TCP connections that are established (you can see the handshakes that starts with `[S]` lines (the SYN request)).

##### Keep-Alive?

In `-t1` scenario, where only one instance of a Jersey client, you can see that based on the keep alive capability of the mockup server, multiple requests are sent through the same TCP connection.

But in `-t2` case, although a client pool is used, actually it looks that a different client is provided at every request for a client from the pool.
Thus, a different TCP connection is created for each and every call from restDataConsumers to httpDataProvider.

