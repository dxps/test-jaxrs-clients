module.exports = MockBase => class MockTasks extends MockBase {

    mocks() {
        return {
            route: '/api/tasks',
            responses: [
                {
                    response: {
                        type: 'json',
                        body: [
                            {
                                "id": 1,
                                "title": "First Task",
                                "description": "This is the first task, having the initial state as planned.",
                                "state": "PLANNED"
                            }, {
                                "id": 2,
                                "title": "Second Task",
                                "description": "The 2nd task of the day.",
                                "state": "POSTPONNED"
                            }, {
                                "id": 3,
                                "title": "Third Thing - Expose /api/tasks as public",
                                "description": "Currently, it is protected.",
                                "state": "DONE"
                            }
                        ]
                    }
                }
            ]
        }
    }
};
