## httpDataProvider

This is a minimal data provider through http. based on http-server node package.

#### Deps

Run `yarn install` or `npm i` to install the main dependency, the http-server node package.

#### Run & Usage

Use `run.sh` script to start it.

Access [http://localhost:8080](http://localhost:8080) to get a simple JSON content.

