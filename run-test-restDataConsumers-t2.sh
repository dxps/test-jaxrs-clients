#!/bin/bash

BEGIN=1
END=100

echo ">>> Calling call-restDataConsumers-t2.sh for $END times ..."

i=$BEGIN
while [[ $i -le $END ]]
do
  echo -n " [$i] " 
  ./call-restDataConsumers-t2.sh | grep "count"
  i=$[i+1];
done

