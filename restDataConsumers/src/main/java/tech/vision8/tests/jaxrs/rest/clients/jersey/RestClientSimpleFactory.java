package tech.vision8.tests.jaxrs.rest.clients.jersey;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import java.util.concurrent.TimeUnit;


/**
 * A simple (close to minimal) factory for creating a REST client.
 *
 * @author vision8
 */
public class RestClientSimpleFactory {

    private Client client;

    public static Client create() {

        return ClientBuilder.newBuilder()
                .connectTimeout(2, TimeUnit.SECONDS)
                .readTimeout(4, TimeUnit.SECONDS)
                .build();
    }

}
