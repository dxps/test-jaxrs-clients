package tech.vision8.tests.jaxrs.services;

import org.apache.http.pool.PoolStats;
import org.glassfish.jersey.apache.connector.ApacheClientProperties;
import org.glassfish.jersey.apache.connector.ApacheConnectorProvider;
import org.glassfish.jersey.client.ClientConfig;

import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.glassfish.jersey.client.ClientProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;


/**
 * A pool of REST clients.
 *
 * @author vision8
 */
@Service
public class JerseyRestClientPool {

    // stability settings
    private int maxPoolSize = 50;
    private int connTimeoutMillis = 600;
    private int readTimeoutMillis = 1500;

    private ClientConfig clientConfig;

    private ClientBuilder clientBuilder;

    private PoolingHttpClientConnectionManager connectionMgr;

    private int getClientCount = 0;

    private static int getClientIntervalReportingStats = 5;

    private static final Logger logger = LoggerFactory.getLogger(JerseyRestClientPool.class);

    /** Create a new instance of this pool. */
    public JerseyRestClientPool() {

        connectionMgr = new PoolingHttpClientConnectionManager();
        connectionMgr.setMaxTotal(maxPoolSize);
        connectionMgr.setDefaultMaxPerRoute(maxPoolSize);

        clientConfig = new ClientConfig()
                // using Apache Connector and its pool implementation
                .connectorProvider(new ApacheConnectorProvider())
                .property(ApacheClientProperties.CONNECTION_MANAGER, connectionMgr)
                .property(ApacheClientProperties.CONNECTION_MANAGER_SHARED, true)
                .property(ClientProperties.CONNECT_TIMEOUT, connTimeoutMillis)
                .property(ClientProperties.READ_TIMEOUT, readTimeoutMillis);

        clientBuilder = ClientBuilder.newBuilder()
                .withConfig(clientConfig);

        logger.debug("Instance #{} created. Stats: {}", this.hashCode(),
                connectionMgr.getTotalStats().toString());
    }

    /** Get a client from the pool. */
    public Client getClient() {

        if (++getClientCount % getClientIntervalReportingStats == 0)
            logger.debug("After {} requests, stats: {}", getClientCount, poolStats());

        return clientBuilder.build();
    }

    /** Get the statistics of the pool. */
    public PoolStats poolStats() {
        return connectionMgr.getTotalStats();
    }

    /** Get the number of calls to <code>getClient()</code> method. */
    public int getGetClientCount() {
        return getClientCount;
    }

}
