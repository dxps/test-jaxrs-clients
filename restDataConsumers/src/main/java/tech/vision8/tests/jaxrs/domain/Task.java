package tech.vision8.tests.jaxrs.domain;


import lombok.Data;


/**
 * This is a simple domain entity.
 *
 * @author vision8
 */
@Data
public class Task {

    private int id;

    private String title;

    private String description;

    private String state;

}
