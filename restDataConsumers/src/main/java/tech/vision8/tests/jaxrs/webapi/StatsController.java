package tech.vision8.tests.jaxrs.webapi;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tech.vision8.tests.jaxrs.services.JerseyRestClientPool;


/**
 * Exposing some statistics through this controller.
 *
 * @author vision8
 */
@RestController
@RequestMapping("/stats")
public class StatsController {

    private static final Logger logger = LoggerFactory.getLogger(StatsController.class);

    private JerseyRestClientPool jerseyRestClientPool;

    public StatsController(JerseyRestClientPool jerseyRestClientPool) {
        this.jerseyRestClientPool = jerseyRestClientPool;
        logger.debug("Using RestClientPool instance #{}", jerseyRestClientPool.hashCode());
    }

    @GetMapping("/jersey-client-pool")
    public String jerseyClientPoolStats() {

        return jerseyRestClientPool.poolStats().toString();
    }

}
