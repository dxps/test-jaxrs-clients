package tech.vision8.tests.jaxrs.webapi;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import tech.vision8.tests.jaxrs.domain.Task;
import tech.vision8.tests.jaxrs.services.JerseyRestClientPool;
import tech.vision8.tests.jaxrs.rest.clients.jersey.RestClientSimpleFactory;

import javax.ws.rs.ProcessingException;
import javax.ws.rs.client.Client;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.concurrent.TimeUnit;


/**
 * The common controller for getting the tasks from the external Test Data Provider component
 * using a simple REST call with the help of different JAX-RS clients.
 *
 * @author vision8
 */
@Controller
public class GetTasksController {

    private Client restClient;
    private JerseyRestClientPool jerseyRestClientPool;

    private int MAX_DELAY_MS = 2000;

    private static final String TASKS_ENDPOINT = "http://localhost:8000";
    private static final String TASKS_PATH = "/api/tasks";

    private static final Logger logger = LoggerFactory.getLogger(GetTasksController.class);

    /** The c'tor used for IoC. */
    public GetTasksController(JerseyRestClientPool jerseyRestClientPool) {
        this.restClient = RestClientSimpleFactory.create();
        this.jerseyRestClientPool = jerseyRestClientPool;
        logger.debug("Using RestClientPool instance #{}", jerseyRestClientPool.hashCode());
    }

    @GetMapping("/tasks-1")
    public String test1(Model model) {

        logger.debug("/tasks-1: Using client {}", restClient.toString());
        List<Task> tasks = restClient
                .target(TASKS_ENDPOINT).path(TASKS_PATH)
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get()
                .readEntity(new GenericType<List<Task>>() {
                });

        delay();

        model.addAttribute("tasks", tasks);

        return "tasks";
    }

    @GetMapping("/tasks-2")
    public String test2(Model model) {

        Client client = jerseyRestClientPool.getClient();
        logger.debug("/tasks-2: Using client #{}", client.hashCode());

        try {
            List<Task> tasks = client
                    .target(TASKS_ENDPOINT).path(TASKS_PATH)
                    .request(MediaType.APPLICATION_JSON_TYPE)
                    .get()
                    .readEntity(new GenericType<List<Task>>() {
                    });

            model.addAttribute("tasks", tasks);

        } catch (ProcessingException pe) {
            logger.error("Cannot get Rest client from the pool! Error: \"{}\". Number of getClient() calls: {}. Pool stats: {}",
                    pe.getMessage(), jerseyRestClientPool.getGetClientCount(), jerseyRestClientPool.poolStats().toString());
            System.exit(2);
        }

        delay();

        return "tasks";
    }

    /** Explicit delay for testing purposes. */
    private void delay() {

        long delay = (long) (Math.random() * MAX_DELAY_MS);
        // logger.debug("{} ms added delay.", delay);
        try {
            TimeUnit.MILLISECONDS.sleep(delay);
        } catch (InterruptedException e) {
            // ignore
        }
    }

}
