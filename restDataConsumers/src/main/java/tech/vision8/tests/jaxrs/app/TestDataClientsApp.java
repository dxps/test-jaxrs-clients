package tech.vision8.tests.jaxrs.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;


/**
 * Application startup.
 *
 * @author vision8
 */
@SpringBootApplication
@ComponentScan(basePackages = { "tech.vision8.tests.jaxrs" })
public class TestDataClientsApp {

    public static void main(String[] args) {
        SpringApplication.run(TestDataClientsApp.class);
    }

}
